#!/usr/bin/make -f
#
# This is free software; see the GNU General Public Licence
# version 2 or later for copying conditions.  There is NO warranty.

include /usr/share/dpkg/pkg-info.mk

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# Always abort if there are any problems with the symbols file.
DPKG_GENSYMBOLS_CHECK_LEVEL = 4
export DPKG_GENSYMBOLS_CHECK_LEVEL

dep_ver := $(shell echo $(DEB_VERSION_UPSTREAM) | cut -d '.' -f -2)

udeb_dir := debian/udeb
udeb_builddir := $(udeb_dir)/build
udeb_installdir := $(udeb_dir)/install
configure_flags := --without-bzip2 --without-harfbuzz

%:
	dh $@

override_dh_autoreconf:
	dh_autoreconf ./autogen.sh

override_dh_auto_configure:
	dh_auto_configure --builddir=$(udeb_builddir) -- $(configure_flags) --without-brotli
	dh_auto_configure -- $(configure_flags)

override_dh_auto_build:
	sed -i '/^#[[:space:]]*define SIZEOF_LONG/s/[0-9]/__SIZEOF_LONG__/' \
	    $(udeb_builddir)/ftconfig.h builds/unix/ftconfig.h
	dh_auto_build --builddir=$(udeb_builddir)
	dh_auto_build
	dh_auto_build --sourcedir=ft2demos -- TOP_DIR=$(CURDIR)

override_dh_auto_install:
	dh_auto_install --builddir=$(udeb_builddir) --destdir=$(udeb_installdir)
	dh_auto_install

override_dh_install:
	dh_install -p libfreetype6-udeb --sourcedir=$(udeb_installdir)
	sed -i -e '/dependency_libs/s/'.*'//' debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libfreetype.la
	dh_install -p freetype2-demos -XREADME
	dh_install --remaining-packages

override_dh_installdocs-indep:
	dh_installdocs -p freetype2-doc --doc-main-package=libfreetype-dev \
	    -Xreference/assets/fonts \
	    -Xreference/assets/images \
	    -Xjs/jquery-1.11.0.min.js
	cp docs/CHANGES debian/freetype2-doc/usr/share/doc/libfreetype-dev/NEWS
	cp src/pcf/README debian/freetype2-doc/usr/share/doc/libfreetype-dev/pcf/

override_dh_installchangelogs:
	dh_installchangelogs -p freetype2-demos ft2demos/ChangeLog
	dh_installchangelogs --remaining-packages

override_dh_makeshlibs:
	dh_makeshlibs -V 'libfreetype6 (>= $(dep_ver))' --add-udeb=libfreetype6-udeb

override_dh_auto_clean:
	dh_auto_clean --sourcedir=ft2demos -- TOP_DIR=$(CURDIR)
	dh_auto_clean
	rm -rf objs/.libs
